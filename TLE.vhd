library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;

library LCD;
use LCD.utils.all;
use LCD.components.all;

library font;
use font.utils.all;
use font.components.all;

library RFID;
use RFID.UART.all;
use RFID.reciever.all;
use RFID.components.all;

library clock;
use clock.components.all;
use clock.utils.all;

library camera;
use camera.components.all;
use camera.utils.all;
use camera.SDRAM.all;

library XESS;
use XESS.CommonPckg.all;
use XESS.SdCardPckg.all;
use work.components.all;
use work.utils.all;

entity TLE is
	port(
		CLK50MHZ		:in std_logic;
	-------------------------LCD out-----------------------
		LCD_R			:out std_logic_vector(7 downto 0);
		LCD_G			:out std_logic_vector(7 downto 0);
		LCD_B			:out std_logic_vector(7 downto 0);
		LCD_RSTB		:out std_logic;
		LCD_MODE		:out std_logic;
		LCD_DCLK		:out std_logic;
		LCD_HSD			:out std_logic;
		LCD_VSD			:out std_logic;
		LCD_SHLR		:out std_logic;
		LCD_UPDN		:out std_logic;
		LCD_DE			:out std_logic;
		LCD_DIM			:out std_logic;
		LCD_DITH		:out std_logic;
		LCD_POWER_CTL	:out std_logic;
	------------------------camera-------------------------
		CAMERA_LVAL		:in std_logic;
		CAMERA_PIXCLK	:in std_logic;
		CAMERA_RESET_N	:out std_logic;
		CAMERA_SCLK		:out std_logic;
		CAMERA_SDATA	:inout std_logic;
		CAMERA_STROBE	:in std_logic;
		CAMERA_TRIGGER	:out std_logic;
		CAMERA_XCLKIN	:out std_logic;
		CAMERA_D		:in std_logic_vector(11 downto 0);
		CAMERA_FVAL		:in std_logic;
	------------------------SDRAM--------------------------
		SDRAM_ADDR	:out std_logic_vector(12 downto 0);
		SDRAM_BA	:out std_logic_vector(1 downto 0);
		SDRAM_CAS_N	:out std_logic;
		SDRAM_CKE	:out std_logic;
		SDRAM_CLK	:out std_logic;
		SDRAM_CS_N	:out std_logic;
		SDRAM_DQ	:inout std_logic_vector(31 downto 0);
		SDRAM_DQM	:out std_logic_vector(3 downto 0);
		SDRAM_RAS_N	:out std_logic;
		SDRAM_WE_N	:out std_logic;
	------------------------RFID---------------------------
		GPIO			:in std_logic_vector(35 downto 0);
	------------------------touch--------------------------
		TOUCH_INT_N		:in std_logic;
		TOUCH_I2C_SCL	:out std_logic;
		TOUCH_I2C_SDA	:inout std_logic;
	----------------------debugging------------------------
		SWITCH			:in std_logic_vector(17 downto 0);
		KEY				:in std_logic_vector(3 downto 0);
		HEX0			:out std_logic_vector(6 downto 0);
		HEX1			:out std_logic_vector(6 downto 0);
		HEX2			:out std_logic_vector(6 downto 0);
		HEX3			:out std_logic_vector(6 downto 0);
		HEX4			:out std_logic_vector(6 downto 0);
		HEX5			:out std_logic_vector(6 downto 0);
		HEX6			:out std_logic_vector(6 downto 0);
		HEX7			:out std_logic_vector(6 downto 0);
		LEDR			:out std_logic_vector(17 downto 0);
		LEDG			:out std_logic_vector(8 downto 0);
	------------------External SD card---------------------
		SD_CLK			:out std_logic;
		SD_CMD			:inout std_logic;
		SD_DAT			:inout std_logic_vector(3 downto 0);
		SD_WP_N			:out std_logic
	);
end entity;

architecture arc of TLE is

--############################################################################
-------------------------------------signals----------------------------------
--############################################################################

	signal reset:std_logic;
	-- various clocks:
	signal clk100mhz:std_logic;
	signal clk200mhz:std_logic;
	signal clk33mhz:std_logic;

	signal touch			:work.utils.touch_type;

	-- LCD.driver outputs meant mainly for text display:
	signal txt				:font.utils.screen_type;

	-- For calculating text display:
	signal char				:font.utils.char_type;
	-- signals to control the cursor:
	signal cursor			:font.utils.cursor_type;

	-- RFID related signals:
	signal RF				:work.utils.RF_type;

	-- signals for managing the camera:
	signal cam				:work.utils.cam_type;
	-- to Mux the different display options:
	signal displaying		:work.utils.displaying_type;

	-- signals for interface with external SD card:
	signal SD				:work.utils.SD_type;

	-- signals for calculating and saving images in SD card:
	signal image			:work.utils.image_type;
	-- hex 7 segment signals: data and segments:

	signal hex				:clock.utils.hex_type;
	-- Configuration for hex7segment:
	for all:hex7segment use entity clock.hex7segment(logic);

begin
	reset<=not KEY(0);

--############################################################################
--------------------------------------text------------------------------------
--############################################################################

	char_proc:process(txt.mode,clk33mhz) --where all the magic begins <3:
			-- There are 16 rows in a character and 8 columns in a character.
			-- for example:
			--	address number 0x0041 {'A' in ascii} contains the glyph:
			--	00 00 00 00 18 24 24 42 42 7E 42 42 42 42 00 00
		variable pc:integer range 0 to chars.portrait-1;
		variable pl:integer range 0 to lines.portrait-1;
		variable lc:integer range 0 to chars.landscape-1;
		variable ll:integer range 0 to lines.landscape-1;
	begin
		if rising_edge(clk33mhz) then
			case txt.mode is
				when portrait=>
							--	the display supposed to be:
							--		----->column            *  *
							--  |   - - - - - - - -      *        *
							--  |   - - - - - - - -     *  camera  *
							--  |   - - - - - - - -     *top--right*
							--  |   - - - - - - - -      *        *
							--  |   - - - # # - - -         *  *
							--  |   - - # - - # - -
							--  |   - - # - - # - -
							--  |   - # - - - - # -
							--  v   - # - - - - # -
							-- row  - # # # # # # -
							--		- # - - - - # -
							--		- # - - - - # -
							--		- # - - - - # -
							--		- # - - - - # -
							--		- - - - - - - -
							--		- - - - - - - -
						/*
						debuging version for calibration of the characters display while the position is fixed by 55 and 25
						char.print<=char.ROM.dataout(
							(conv_integer(txt.pixel.row(3 downto 0))-conv_integer(debuger(4 downto 1)))*8+
							(conv_integer(txt.pixel.column(2 downto 0))+conv_integer(debuger(7 downto 5)))mod 8
						);
						pc:=((conv_integer(not txt.pixel.column)+conv_integer(debuger(10 downto 8)))/8+
							55)mod(chars.portrait);
						pl:=((conv_integer(not txt.pixel.row)-conv_integer(debuger(14 downto 11)))/16+
							25)mod(lines.portrait);
						char.code<=char.portrait(index.portrait.c,index.portrait.l);
						*/
					char.print<=char.ROM.dataout(
						(conv_integer(txt.pixel.row(3 downto 0))-1)*8+
						(conv_integer(txt.pixel.column(2 downto 0))+0)mod 8
					);
					char.RAM.portrait.read.address.c<=((conv_integer(not txt.pixel.column)+0)/8+ 55)mod(chars.portrait);
					char.RAM.portrait.read.address.l<=((conv_integer(not txt.pixel.row)-2)/16+25)mod(lines.portrait);
				when landscape=>
							--	when mode is landscape the glyph should be displayed like that:
							--     *  *         ----->row
							--  *        *  |   - - - - - -	- -
							-- *  camera  * |   - - - - - -	- -
							-- * top-left * |   - - - - - -	- -
							--  *        *  |   - - - - - -	- -
							--     *  *     |   - - - # # -	- -
							--              |   - - # - - # - -
							--              |   - - # - - # - -
							--              |   - # - - - - # -
							--              v   - # - - - - # -
							--            column- # # # # # # -
							--                  - # - - - - # -
							--                  - # - - - - # -
							--                  - # - - - - # -
							--                  - # - - - - # -
							--                  - - - - - - - -
							--                  - - - - - - - -
						/*
						debuging version for calibration of the characters display while the position is fixed by 49	and 29
						char.print<=char.ROM.dataout(
							(conv_integer(not txt.pixel.column(3 downto 0))+	conv_integer(debuger(4 downto 1)))*8+
							(conv_integer(txt.pixel.row(2 downto 0))+		conv_integer(debuger(7 downto 5)))mod 8
						);
						lc:=((conv_integer(not txt.pixel.row)+ conv_integer(debuger(10 downto 08)))/8+
							49)mod(chars.landscape);
						ll:=((conv_integer(txt.pixel.column)-	 conv_integer(debuger(14 downto 11)))/16+
							29)mod(lines.landscape);
						increment `not txt.pixel.row` to change characters' position to the left
						decrement `txt.pixel.column` to lower characters' position down
						*/
					char.print<=char.ROM.dataout(
						(conv_integer(not txt.pixel.column(3 downto 0))+1)*8+
						(conv_integer(txt.pixel.row(2 downto 0))+5)mod 8
					);
					char.RAM.landscape.read.address.c<=((conv_integer(not txt.pixel.row)+ 2)/8+49)mod(chars.landscape);
					char.RAM.landscape.read.address.l<=((conv_integer(txt.pixel.column)- 2)/16+29)mod(lines.landscape);
			end case;
			char.RAM.portrait.read.enable<='1';
		end if;
	end process;
	char_ROM_inst:font.components.char_ROM
		port map(
			address=>char.ROM.address,
			clock=>CLK50MHZ,
			q=>char.ROM.dataout
	);
	char_RAM_portrait_inst:font.components.char_RAM_portrait
		port map(
			clock		=>clk33mhz,
			data		=>char.RAM.portrait.write.data,
			rdaddress	=>char.RAM.portrait.read.address,
			rden		=>char.RAM.portrait.read.enable,
			wraddress	=>char.RAM.portrait.write.address,
			wren		=>char.RAM.portrait.write.enable,
			q			=>char.ROM.address
	);

--############################################################################
--------------------------------------main------------------------------------
--############################################################################

	/*
	main_proc:process(clk33mhz,reset)
		type state_sub_type is (
			printing,
			input
		);
		type state_main_type is (
			intro,
			passwd,
			RFID,
			image
		);
		type state_type is record
			main:state_main_type;
			sub	:state_sub_type;
		end record;
		variable state:state_type;
		variable pl:integer range 0 to lines.portrait-1;
		variable pc:integer range 0 to chars.portrait-1;
		variable char2print:character;
		variable ready:boolean;
		procedure print(
			str:string(positive range<>)
		)is
		begin
			case pc is
				when 0 to str'length-1=>
					char2print:=str(pc+1);
				when others=>
					char2print:=' ';
			end case;
			if pc<str'length then
				pc:=pc+1;
				ready:=false;
				char.RAM.portrait.write.enable<='1';
			else
				pc:=0;
				char.RAM.portrait.write.enable<='0';
				ready:=true;
			end if;
		end procedure;
	begin
		char.RAM.portrait.write.data<=char2ascii(char2print);
		char.RAM.portrait.write.address.c<=pc;
		char.RAM.portrait.write.address.l<=pl;
		if reset='1' then
			--clear;
			cursor.portrait.c<=10;
			cursor.portrait.l<=2;
			cursor.blink<=true;
			cursor.show<=true;
			txt.mode<=portrait;
			displaying<=log;
			--temporary disable all text prints to reduce memory usage:
			state.main:=intro;
			pl:=0;
			pc:=0;
		elsif rising_edge(clk33mhz) then
			case state.main is
				when intro=>
					case pl is
						when 0=>
							print("Hello, this is an RFID-based entry-permissions system,");
						when 1=>
							print("If you would like to set it up, enter a 4 digits code");
						when 2=>
							print("Password:");
						when others=>
							-- Moving out to the next state:
							state.main:=passwd;
							-- We print a keyboard in the next state so we need to make sure
							-- the 1st line we will be printing is going to be the one before
							-- last:
							pl:=lines.portrait-2;
							-- Make sure we reach the next main state while ready is false so
							-- We won't end up with a keyboard printed 2 rows above our intention:
							ready:=false;
							-- Make sure we reach the next main state while we are in printing mode:
							state.sub:=printing;
					end case;
					if ready then
						pl:=pl+1;
					end if;
				when passwd=>
					case state.sub is
						when printing=>
							case pl is
								when lines.portrait-2-3*0=>
									print(keyboard_layout.portrait.numbers(2));
								when lines.portrait-2-3*1=>
									print(keyboard_layout.portrait.numbers(1));
								when lines.portrait-2-3*2=>
									print(keyboard_layout.portrait.numbers(0));
								when others=>
									ready:=false;
									pl:=4;
									state.sub:=input;
							end case;
							if ready then
								pl:=pl-3;
							end if;
						when input=>
							if touch.fingers="01" and touch.active.current='1' and touch.active.last='0' then
								if touch.delayer<6 then
									touch.delayer<=touch.delayer+1;
								else
									touch.delayer<=0;
									-- Temporarly disable the passwd input and move
									-- to the next main state to avoid the state machine
									-- being stuck:
									state.main:=RFID;
									state.sub:=printing;
								end if;
							end if;
							touch.active.last<=touch.active.current and touch.fingers(0);
					end case;
				when RFID=>
					case state.sub is
						when printing=>
							case pl is
								when 4=>
									print("great, now put an RFID tag close to the reader and wait for");
								when 5=>
									print("a response..");
								when others=>
									state.sub:=input;
									pl:=6;
									ready:=false;
							end case;
							if ready then
								pl:=pl+1;
							end if;
						when input=>
							if RF.valid='1' then
								state.main:=image;
								state.sub:=printing;
							end if;
					end case;
				when image=>
					case state.sub is
						when printing=>
							case pl is
								when 6=>
									print("RFID reading was successful.");
								when 7=>
									print("Now, In 5 seconds you will have to tap the screen");
								when 8=>
									print("In order to take a picture");
								when others=>
									state.sub:=input;
									pl:=9;
									ready:=false;
							end case;
							if ready then
								pl:=pl+1;
							end if;
						when input=>
					end case;
			end case;
		end if;
		case state.main is
			when intro	=>hex.data(0)<="00000";
			when passwd	=>hex.data(0)<="00001";
			when RFID	=>hex.data(0)<="00010";
			when image	=>hex.data(0)<="00011";
		end case;
		case state.sub is
			when printing	=>hex.data(1)<="00000";
			when input		=>hex.data(1)<="00001";
		end case;
	end process;
	*/

--############################################################################
-------------------------------------touch------------------------------------
--############################################################################

	i2c_touch_config_inst:LCD.components.i2c_touch_config
		port map(
			iCLK=>CLK50MHZ,
			iRSTN=>not reset,
			iTRIG=>not TOUCH_INT_N,
			I2C_SCLK=>TOUCH_I2C_SCL,
			I2C_SDAT=>TOUCH_I2C_SDA,
			oREADY=>touch.active.current,
			oREG_X1=>touch.row(0),
			oREG_Y1=>touch.column(0),
			oREG_X2=>touch.row(1),
			oREG_Y2=>touch.column(1),
			oREG_GESTURE=>touch.gesture,
			oREG_TOUCH_COUNT=>touch.fingers
	);

--############################################################################
-------------------------------------clocks-----------------------------------
--############################################################################

	clk_div_cursor:clock.components.div
		generic map(
			divide_by=>25000000
		)
		port map(
			clkout=>cursor.clk,
			clkin=>CLK50MHZ,
			reset=>reset
	);
	PLL_inst:work.components.PLL
		port map(
			areset	=>reset,
			inclk0	=>CLK50MHZ,
		-- clock for LCD driver.
			c0		=>clk33mhz,
		-- clock for both RFID-reader and SD-card controller + the controller for the SDRAM.
			c1		=>clk100mhz,
		-- Clock for camera - 25mhz
			c2		=>CAMERA_XCLKIN,
		-- clock for SDRAM - 100mhz with -105.00 degrees clock phase shift.
			c3		=>SDRAM_CLK
	);

--############################################################################
-------------------------------------images-----------------------------------
--############################################################################

	--auto start when power on
	cam.auto_start <= '1' when ((KEY(0)) and (cam.reset(3)) and (not cam.reset(4)))='1' else '0';
	--reset signals
	camera_reset_delay_inst:camera.components.reset_delay
		port map(
			iCLK		=>CLK50MHZ,
			iRST		=>KEY(0),
			oRST_0		=>cam.reset(0),
			oRST_1		=>cam.reset(1),
			oRST_2		=>cam.reset(2),
			oRST_3		=>cam.reset(3), --auto-start start point
			oRST_4		=>cam.reset(4)  --auto-start end point
	);
	CCD_reg_proc:process(CAMERA_PIXCLK)
	begin
		if rising_edge(CAMERA_PIXCLK) then
			cam.CCD.reg.DATA	<=	CAMERA_D;
			cam.CCD.reg.LVAL	<=	CAMERA_LVAL;
			cam.CCD.reg.FVAL	<=	CAMERA_FVAL;
		end if;
	end process;
	CAMERA_TRIGGER	<='1';  -- TRIGGER
	CAMERA_RESET_N	<=cam.reset(1);
	camera_CCD_capture_inst:camera.components.CCD_capture
		port map(
			oDATA		=>cam.CCD.m.DATA,
			oDVAL		=>cam.CCD.m.DVAL,
			oX_Cont		=>cam.count.x,
			oY_Cont		=>cam.count.y,
			oFrame_Cont	=>cam.count.frame,
			iDATA		=>cam.CCD.reg.data,
			iFVAL		=>cam.CCD.reg.FVAL,
			iLVAL		=>cam.CCD.reg.LVAL,
			iSTART		=>(not cam.ctrl.end_capture) or cam.auto_start,
			iEND		=>not cam.ctrl.capture,
			iCLK		=>not CAMERA_PIXCLK,
			iRST		=>cam.reset(2)
	);
	camera_I2C_CCD_config_inst:camera.components.I2C_CCD_config
		port map(
			-- Host Side
			iCLK			=>CLK50MHZ,
			iRST_N			=>cam.reset(2),
			iEXPOSURE_ADJ	=>cam.ctrl.exposure_adjust,
			iEXPOSURE_DEC_p	=>cam.ctrl.exposure_dec_p,
			iMIRROR_SW		=>cam.ctrl.invert_color,
			-- I2C Side
			I2C_SCLK		=>CAMERA_SCLK,
			I2C_SDAT		=>CAMERA_SDATA
	);
	--frame buffer
	camera_SDRAM_control_inst:camera.components.SDRAM_control
		port map(
		-- HOST Side
			RESET_N			=>KEY(0),
			CLK				=>clk100mhz,
		-- FIFO Write Side 1
			WR1_DATA		=>'0'&cam.CCD.s.green(11 downto 7)&cam.CCD.s.blue(11 downto 2),
			WR1				=>cam.CCD.s.DVAL,
			WR1_ADDR		=>(others=>'0'),
			WR1_MAX_ADDR	=>conv_std_logic_vector(800*480/2,ASIZE),
			WR1_LENGTH		=>x"80",
			WR1_LOAD		=>not cam.reset(0),
			WR1_CLK			=>CAMERA_PIXCLK,
		-- FIFO Write Side 2
			WR2_DATA		=>'0'&cam.CCD.s.green(6 downto 2)&cam.CCD.s.red(11 downto 2),
			--.WR2_DATA({6'b000000,10'b1111111111}),
			WR2				=>cam.CCD.s.DVAL,
			WR2_ADDR		=>conv_std_logic_vector(x"100000",23),
			WR2_MAX_ADDR	=>conv_std_logic_vector(x"100000",23)+conv_std_logic_vector(800*480/2,ASIZE),
			WR2_LENGTH		=>x"80",
			WR2_LOAD		=>not cam.reset(0),
			WR2_CLK			=>CAMERA_PIXCLK,
		-- FIFO Read Side 1
			RD1_DATA		=>cam.read.data(0),
			RD1				=>cam.read.now,
			RD1_ADDR		=>(others=>'0'),
			RD1_MAX_ADDR	=>conv_std_logic_vector(800*480/2,ASIZE),
			RD1_LENGTH		=>x"80",
			RD1_LOAD		=>not cam.reset(0),
			RD1_CLK			=>not clk33mhz,
		-- FIFO Read Side 2
			RD2_DATA		=>cam.read.data(1),
			RD2				=>cam.read.now,
			RD2_ADDR		=>conv_std_logic_vector(x"100000",ASIZE),
			RD2_MAX_ADDR	=>conv_std_logic_vector(x"100000",ASIZE)+conv_std_logic_vector(800*480/2,ASIZE),
			RD2_LENGTH		=>x"80",
			RD2_LOAD		=>not cam.reset(0),
			RD2_CLK			=>not clk33mhz,
		-- SDRAM Side
			SA				=>SDRAM_ADDR(11 downto 0),
			BA				=>SDRAM_BA,
			CS_N(0)			=>SDRAM_CS_N,
			CS_N(1)			=>open,
			CKE				=>SDRAM_CKE,
			RAS_N			=>SDRAM_RAS_N,
			CAS_N			=>SDRAM_CAS_N,
			WE_N			=>SDRAM_WE_N,
			DQ				=>SDRAM_DQ,
			DQM				=>SDRAM_DQM
	);
	camera_RAW2RGB_inst:camera.components.RAW2RGB
		port map(
			iCLK		=>CAMERA_PIXCLK,
			iRST_n		=>cam.reset(1),
			iData		=>cam.CCD.m.data,
			iDval		=>cam.CCD.m.DVAL,
			oRed		=>cam.CCD.s.red,
			oGreen		=>cam.CCD.s.green,
			oBlue		=>cam.CCD.s.blue,
			oDval		=>cam.CCD.s.DVAL,
			iMIRROR		=>cam.ctrl.invert_color,
			iX_Cont		=>cam.count.x,
			iY_Cont		=>cam.count.y
	);

	camera_LCD_controller_inst:camera.components.LCD_controller
		port map(
			clk33mhz	=>clk33mhz,
			reset		=>not cam.reset(2),
		-- LCD side:
			HSD			=>cam.display.HSD,
			VSD			=>cam.display.VSD,
			pixel		=>cam.display.pixel
	);
	process(cam.display.pixel)
	begin
		if
		and cam.display.pixel.row>=LCD.utils.horizontal.PW+LCD.utils.horizontal.BP
		and cam.display.pixel.row<LCD.utils.horizontal.P-LCD.utils.horizontal.FP
		and cam.display.pixel.column>=LCD.utils.vertical.PW+LCD.utils.vertical.BP
		and cam.display.pixel.column<LCD.utils.vertical.P-LCD.utils.vertical.FP
		then
			cam.display.red		<=cam.read.data(1)(9 downto 2);
			cam.display.green	<=cam.read.data(0)(14 downto 10)&cam.read.data(1)(14 downto 12);
			cam.display.blue	<=cam.read.data(0)(9 downto 2);
			cam.enable<='1';
			cam.read.now<='1';
		else
			cam.display.red		<=(others=>'0');
			cam.display.green	<=(others=>'0');
			cam.display.blue	<=(others=>'0');
			cam.enable<='0';
			cam.read.now<='0';
		end if;
	end process;

	-- these use to be connected to KEYS in camera/driver.vhd:
	cam.ctrl.exposure_adjust<='1';
	-- the same with these signals and switches:
	cam.ctrl.invert_color<='0';
	cam.ctrl.exposure_dec_p<='0';

	image_proc:process(clk33mhz,reset)
		-- User input:
		variable k1,k2	:work.utils.button_type;
		type state_type is (wait4user,wait4enable2rise,capturing);
		variable state	:state_type;
	begin
		k1.current:=KEY(2);
		k2.current:=cam.enable;
		cam.ctrl.capture<=k1.current;
		if reset='1' then
			state:=wait4user;
			image.RAM.wren<='0';
			cam.ctrl.end_capture<='1';
		elsif rising_edge(clk33mhz) then
			case state is
				when wait4user=>
					if k1.last='0' and k1.current='1' then
						state:=wait4enable2rise;
						cam.ctrl.end_capture<='1';
					end if;
					k1.last:=k1.current;
				when wait4enable2rise=>
					if k2.last='0' and k2.current='1' then
						state:=capturing;
						image.RAM.wren<='1';
					end if;
					k2.last:=k2.current;
				when capturing=>
					if
					cam.enable='1' and cam.display.HSD='1' and cam.display.VSD='1'
					and cam.display.pixel.row>=LCD.utils.horizontal.PW+LCD.utils.horizontal.BP+2*800/3
					and cam.display.pixel.row<LCD.utils.horizontal.P-LCD.utils.horizontal.FP
					and cam.display.pixel.column>=LCD.utils.vertical.PW+LCD.utils.vertical.BP
					and cam.display.pixel.column<LCD.utils.vertical.P-LCD.utils.vertical.FP
					then
						image.RAM.data<=cam.display.red&cam.display.green&cam.display.blue;
						if image.RAM.wraddress<480*800/3 then
							image.RAM.wraddress<=image.RAM.wraddress+1;
						else
							image.RAM.wraddress<=(others=>'0');
							image.RAM.wren<='0';
							cam.ctrl.end_capture<='0';
							state:=wait4user;
							image.RAM.wren<='0';
						end if;
					end if;
			end case;
		end if;
	end process;

	image_RAM_inst:work.components.image_RAM
		port map(
			data	 	=>image.RAM.data,
			rdaddress	=>image.RAM.rdaddress,
			rdclock		=>clk33mhz,
			rden		=>image.RAM.rden,
			wraddress	=>image.RAM.wraddress,
			wrclock		=>clk33mhz,
			wren	 	=>image.RAM.wren,
			q	 		=>image.RAM.q
	);

	reading_proc:process(clk33mhz,reset)
	begin
		if reset='1' then
			image.RAM.rdaddress<=(others=>'0');
			image.RAM.rden<='0';
		elsif rising_edge(clk33mhz) then
			if switch(16)='1' then
				displaying<=img;
				image.RAM.rden<='1';
				if
				cam.enable='1' and cam.display.HSD='1' and cam.display.VSD='1'
				and cam.display.pixel.row>=LCD.utils.horizontal.PW+LCD.utils.horizontal.BP+2*800/3
				and cam.display.pixel.row<LCD.utils.horizontal.P-LCD.utils.horizontal.FP
				and cam.display.pixel.column>=LCD.utils.vertical.PW+LCD.utils.vertical.BP
				and cam.display.pixel.column<LCD.utils.vertical.P-LCD.utils.vertical.FP
				then
					if image.RAM.rdaddress<480*800/3 then
						image.RAM.rdaddress<=image.RAM.rdaddress+1;
						image.red<=image.RAM.q(7 downto 0);
						image.green<=image.RAM.q(15 downto 8);
						image.blue<=image.RAM.q(23 downto 16);
					else
						image.RAM.rdaddress<=(others=>'0');
					end if;
				else
					image.red<=(others=>'0');
					image.green<=(others=>'0');
					image.blue<=(others=>'0');
				end if;
			else
				image.RAM.rden<='0';
				displaying<=capture;
			end if;
		end if;
	end process;

	SdCardCtrl_inst:xess.SdCardCtrl
		generic map(
			FREQ_G			=>100.0,		-- real			default=100.0;		- Master clock frequency (MHz).
			INIT_SPI_FREQ_G =>0.4,			-- real			default=0.4;		- Slow SPI clock freq. during initialization (MHz).
			SPI_FREQ_G		=>25.0,			-- real			default=25.0;		- Operational SPI freq. to the SD card (MHz).
			BLOCK_SIZE_G	=>512,			-- natural		default=512;		- Number of bytes in an SD card block or sector.
			CARD_TYPE_G		=>SDHC_CARD_E	-- CardType_t	default=SD_CARD_E	- Type of SD card connected to this controller.
		)
		port map(
		-- Host-side interface signals:
			clk_i		=>clk100mhz,		-- Master clock.
			reset_i		=>SD.reset,			-- active-high, synchronous  reset.
			rd_i		=>SD.read,			-- active-high read block request.
			wr_i		=>SD.write,			-- active-high write block request.
			continue_i	=>SD.continue,		-- If true, inc address and continue R/W.
			addr_i		=>SD.address,		-- Block address.
			data_i		=>SD.datain,		-- Data to write to block.
			data_o		=>SD.dataout,		-- Data read from block.
			busy_o		=>SD.busy,			-- High when controller is busy performing some operation.
			hndShk_i	=>SD.HS.i,	-- High when host has data to give or has taken data.
			hndShk_o	=>SD.HS.o,	-- High when controller has taken data or has data to give.
			error_o		=>SD.err,
		-- I/O signals to the external SD card:
			cs_bo		=>SD_DAT(3),		-- Active-low chip-select.
			sclk_o		=>SD_clk,			-- Serial clock to SD card.
			mosi_o		=>SD_cmd,			-- Serial data output to SD card.
			miso_i		=>SD_DAT(0)			-- Serial data input from SD card.
	);

	LEDR(15 downto 0)<=SD.err;
	SD_proc:process(clk33mhz,reset)
		type state_init_type is (rst,wait4busy,wait4response);
		-- read/write state type:
		type state_rw_type is (wait4HSU,byte,HSU,wait4HSD,HSD);
			-- HSD = handshake down I/O
			-- HSU = handshake up I/O
		type state_main_type is (init,idle,writing,reading);
		type state_type is record
			main:state_main_type;
			init:state_init_type;
			rd:state_rw_type;
			wr:state_rw_type;
		end record;
		variable state:state_type;
		variable k:work.utils.button_type;
	begin
		k.current:=KEY(3);
		if reset='1' then
			SD.reset<='1';
			state.main:=init;
			state.init:=rst;
			state.wr:=wait4HSU;
			state.rd:=wait4HSU;
			SD.address<=(others=>'0');
			LEDG(7 downto 0)<=(others=>'0');
		elsif rising_edge(clk33mhz) then
			case state.main is
				when init=>
					case state.init is
						when rst=>
							SD.reset<='0';
							state.init:=wait4response;
						when wait4response=>
							if SD.err=x"00" then
								state.init:=wait4busy;
							end if;
						when wait4busy=>
							if SD.busy='0' then
								state.main:=idle;
							end if;
					end case;
				when idle=>
					if k.current/=k.last then
						if SWITCH(17)='1' then
							state.main:=writing;
							SD.read<='0';
							SD.write<='1';
							state.wr:=wait4HSU;
							LEDG(7 downto 0)<=(others=>'0');
						else
							state.main:=reading;
							SD.read<='1';
							SD.write<='0';
							state.rd:=wait4HSU;
						end if;
					end if;
					k.last:=k.current;
				when writing=>
					SD.read<='0';
					SD.write<='0';
					case state.wr is
						when wait4HSU=>
							if SD.HS.o='1' and SD.busy='1' then
								state.wr:=byte;
							elsif SD.HS.o='0' and SD.busy='0' then
								state.main:=idle;
							end if;
						when byte=>
							SD.datain<=SWITCH(7 downto 0);
							state.wr:=HSU;
						when HSU=>
							SD.HS.i<='1';
							state.wr:=wait4HSD;
						when wait4HSD=>
							if SD.HS.o='0' then
								state.wr:=HSD;
							end if;
						when HSD=>
							SD.HS.i<='0';
							state.wr:=wait4HSU;
					end case;
				when reading=>
					SD.read<='0';
					SD.write<='0';
					case state.rd is
						when wait4HSU=>
							if SD.HS.o='1' and SD.busy='1' then
								state.rd:=byte;
							elsif SD.HS.o='0' and SD.busy='0' then
								state.main:=idle;
							end if;
						when byte=>
							LEDG(7 downto 0)<=SD.dataout;
							state.rd:=HSU;
						when HSU=>
							SD.HS.i<='1';
							state.rd:=wait4HSD;
						when wait4HSD=>
							if SD.HS.o='0' then
								state.rd:=HSD;
							end if;
						when HSD=>
							SD.HS.i<='0';
							state.rd:=wait4HSU;
					end case;
			end case;
		end if;
	end process;

--############################################################################
-------------------------------------cursor-----------------------------------
--############################################################################

	cursor_proc:process(cursor)
		variable C:C_type;
	begin
		C.portrait.c.small:=LCD.utils.vertical.P-LCD.utils.vertical.PW-LCD.utils.vertical.BP-cursor.portrait.c*8-8;
		C.portrait.c.big:=LCD.utils.vertical.P-LCD.utils.vertical.PW-LCD.utils.vertical.BP-cursor.portrait.c*8;
		C.portrait.l.small:=LCD.utils.horizontal.P-LCD.utils.horizontal.FP-cursor.portrait.l*16-16;
		C.portrait.l.big:=LCD.utils.horizontal.P-LCD.utils.horizontal.FP-cursor.portrait.l*16;
		C.landscape.c.small:=LCD.utils.horizontal.P-LCD.utils.horizontal.PW-LCD.utils.horizontal.BP-cursor.landscape.c*8-8;
		C.landscape.c.big:=LCD.utils.horizontal.P-LCD.utils.horizontal.PW-LCD.utils.horizontal.BP-cursor.landscape.c*8;
		C.landscape.l.small:=LCD.utils.vertical.FP+cursor.landscape.l*16;
		C.landscape.l.big:=LCD.utils.vertical.FP+cursor.landscape.l*16+16;
		if cursor.show and displaying=log then
			case txt.mode is
				when portrait=>
					if txt.pixel.row>C.portrait.l.small and txt.pixel.row<C.portrait.l.big and txt.pixel.column>C.portrait.c.small and txt.pixel.column<C.portrait.c.big then
						if cursor.blink then
							cursor.print<=cursor.clk;
						else
							cursor.print<='1';
						end if;
					else
						cursor.print<='0';
					end if;
				when landscape=>
					if txt.pixel.row>C.landscape.l.small and txt.pixel.row<C.landscape.l.big and txt.pixel.column>C.landscape.c.small and txt.pixel.column<C.landscape.c.big then
						if cursor.blink then
							cursor.print<=cursor.clk;
						else
							cursor.print<='1';
						end if;
					else
						cursor.print<='0';
					end if;
			end case;
		else
			cursor.print<='0';
		end if;
	end process;

--############################################################################
--------------------------------------misc------------------------------------
--############################################################################

	LCD_driver_inst:LCD.main.driver
		port map(
			clk33mhz=>clk33mhz,
			reset=>reset,
			HSD=>txt.HSD,
			VSD=>txt.VSD,
			pixel=>txt.pixel
	);
	digits2hex7segment:for i in 0 to 7 generate
		hex7segment_inst:clock.components.hex7segment
			port map(
				data=>hex.data(i),
				segments=>hex.segments(i)
		);
	end generate digits2hex7segment;
	HEX0<=hex.segments(0);
	HEX1<=hex.segments(1);
	HEX2<=hex.segments(2);
	HEX3<=hex.segments(3);
	HEX4<=hex.segments(4);
	HEX5<=hex.segments(5);
	HEX6<=hex.segments(6);
	HEX7<=hex.segments(7);
	RFID_reader_inst:RFID_reader
		port map(
			data		=>RF.data,
			clk100mhz	=>clk100mhz,
			reset		=>reset,
			ID			=>RF.ID.current,
			successful	=>RF.valid,
			broadcast	=>RF.broadcast.current_state,
			SIM_vars	=>open,
			byte		=>open,
			uart_tx		=>open,
			pmod		=>open
	);
	RF.data<=not GPIO(10);
	LCD_proc:process(displaying)
	begin
		LCD_DCLK	<=clk33mhz;
		LCD_RSTB	<='1';
		LCD_MODE	<='0';
		LCD_DIM		<='1';
		LCD_UPDN	<='0';
		LCD_POWER_CTL<='1';
		case displaying is
			when capture=>
				LCD_R		<=cam.display.red;
				LCD_G		<=cam.display.green;
				LCD_B		<=cam.display.blue;
				LCD_HSD		<=cam.display.HSD;
				LCD_VSD		<=cam.display.VSD;
				LCD_SHLR	<='1';
				LCD_DE		<=cam.enable;
				LCD_DITH	<='0';
			when log=>
				LCD_R		<=(others=>cursor.print or char.print);
				LCD_G		<=(others=>cursor.print or char.print);
				LCD_B		<=(others=>cursor.print or char.print);
				LCD_HSD		<=txt.HSD;
				LCD_VSD		<=txt.VSD;
				LCD_SHLR	<='0';
				LCD_DE		<='1';
				LCD_DITH	<='1';
			when img=>
				LCD_R		<=image.red;
				LCD_G		<=image.green;
				LCD_B		<=image.blue;
				LCD_HSD		<=cam.display.HSD;
				LCD_VSD		<=cam.display.VSD;
				LCD_SHLR	<='1';
				LCD_DE		<=cam.enable;
				LCD_DITH	<='0';
		end case;
	end process;

end architecture;
