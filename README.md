# RFID entry permissions management system with FPGA.
This is a personal Project of mine. It uses an [RFID reader by Parallax #28140](https://www.parallax.com/product/28140) and the [VEEK-MT by Terasic](http://www.terasic.com.tw/cgi-bin/page/archive.pl?CategoryNo=139&Language=English&No=670) with the [DE2-115](http://wl.altera.com/education/univ/materials/boards/de2-115/unv-de2-115-board.html) by Altera. It's not truly done yet because the original requirements for it to be considered done are:
- Write a driver for the screen that enables writing text.
- Write a driver for the camera.
- Operate external memory with reading/writing options for RFID tags that are authorized and unauthorized to enter building.

I finished the 1st part. Checkout [`LCD/`](https://github.com/Doron-Behar/VEEK-MT_LCD-driver) for a better look at the driver for the LCD and how it works. Besides the part of the text, I use a controller for an external SD card made by [XESS corp](xess.com). I tested it and it works great but I haven't yet connected it to the camera. **The hard part** with the project is the camera and processing the image. Currently I use a forked driver by Terasic for this task.

### Branches
- [`image`](https://github.com/Doron-Behar/RFID-entry-permissions-sys/tree/image) is meant to be a work stage for all the work regarding the image processing.
- [`presentation`](https://github.com/Doron-Behar/RFID-entry-permissions-sys/tree/presentation) is just a branch I created and pushed in which I don't operate the camera but I show on the screen messages for users of the RFID. I have 3 tags with which I can test the RFID reader and I created 3 different messages for each one of this tags in branch [`presentation`](https://github.com/Doron-Behar/RFID-entry-permissions-sys/tree/presentation). As you might imagine, It's not implemented with anything that resembles a data base within the FPGA and there is no support for adding more valid users - **It's just a case statement**.

### License
The license for this code is [GPL version 2](http://www.bealto.com/fpga-uart.html).

### Contributing
Read [`LCD/CONTRIBUTING.md`](https://github.com/Doron-Behar/VEEK-MT_LCD-driver/blob/master/CONTRIBUTING.md)

### Notes
- I use Quartus, Checkout [LCD/CONTRIBUTING.md](https://github.com/Doron-Behar/VEEK-MT_LCD-driver/blob/master/CONTRIBUTING.md) for more information about my work flow.
- The RFID reader is sending the data in a UART protocol. [RFID/](https://github.com/Doron-Behar/parallax-28140-RFID-reader) is a repo of mine as well but it is based upon a [fork of mine for a basic UART controller.](https://github.com/Doron-Behar/forked-basic-UART). The original code for the RFID reader was extracted from [here](http://www.bealto.com/fpga-uart.html).
