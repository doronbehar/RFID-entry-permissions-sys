# camera:
# - libraries:
set_global_assignment -name VHDL_FILE camera/lib/SDRAM.vhd -library camera
set_global_assignment -name SOURCE_FILE camera/SDRAM/parameters.h -library camera
set_global_assignment -name VHDL_FILE camera/lib/components.vhd -library camera
set_global_assignment -name VHDL_FILE camera/lib/utils.vhd -library camera
# - entities:
set_global_assignment -name QIP_FILE camera/SDRAM/WRFIFO.qip -library camera
set_global_assignment -name QIP_FILE camera/SDRAM/RDFIFO.qip -library camera
set_global_assignment -name VERILOG_FILE camera/SDRAM/control.v -library camera
set_global_assignment -name VERILOG_FILE camera/SDRAM/data-path.v -library camera
set_global_assignment -name VERILOG_FILE camera/SDRAM/control-interface.v -library camera
set_global_assignment -name VERILOG_FILE camera/SDRAM/command-control.v -library camera
set_global_assignment -name VERILOG_FILE camera/reset-delay.v -library camera
set_global_assignment -name VERILOG_FILE camera/RAW2RGB.v -library camera
set_global_assignment -name QIP_FILE camera/line_buffer.qip -library camera
set_global_assignment -name VERILOG_FILE camera/I2C/controller.v -library camera
set_global_assignment -name VERILOG_FILE camera/I2C/CCD-config.v -library camera
set_global_assignment -name VERILOG_FILE camera/CCD-capture.v -library camera
set_global_assignment -name VHDL_FILE camera/LCD-controller.vhd -library camera

# LCD:
# - libraries:
set_global_assignment -name VHDL_FILE LCD/lib/utils.vhd -library LCD
set_global_assignment -name VHDL_FILE LCD/lib/components.vhd -library LCD
set_global_assignment -name VHDL_FILE LCD/lib/font.vhd -library font
# - entities:
set_global_assignment -name VERILOG_FILE LCD/i2c_touch_config.v -library LCD
set_global_assignment -name VHDL_FILE LCD/driver.vhd -library LCD
set_global_assignment -name VHDL_FILE LCD/char.ROM.vhd -library font
set_global_assignment -name VHDL_FILE LCD/char.RAM.vhd -library font

# RFID:
# - libraries:
set_global_assignment -name VHDL_FILE RFID/lib/UART.vhd -library RFID
set_global_assignment -name VHDL_FILE RFID/lib/receiver.vhd -library RFID
set_global_assignment -name VHDL_FILE RFID/lib/components.vhd -library RFID
# - entities:
set_global_assignment -name VHDL_FILE RFID/reader.vhd -library LCD

# work:
# - libraries:
set_global_assignment -name VHDL_FILE lib/utils.vhd
set_global_assignment -name VHDL_FILE lib/components.vhd
# - entities:
set_global_assignment -name QIP_FILE PLL.qip
set_global_assignment -name VHDL_FILE TLE.vhd
set_global_assignment -name QIP_FILE image_RAM.qip

# clock:
# - libraries:
set_global_assignment -name VHDL_FILE clock/lib/utils.vhd -library clock
set_global_assignment -name VHDL_FILE clock/lib/components.vhd -library clock
# - entities:
set_global_assignment -name VHDL_FILE clock/hex7segment.vhd -library clock
set_global_assignment -name VHDL_FILE clock/div.vhd -library clock

# Xess:
set_global_assignment -name VHDL_FILE xess-VHDL-lib/Common.vhd -library XESS
# Note: Don't add xess-VHDL-lib/ClkGen.vhd to XESS library. It has no use in our
# design. In order to fix errors about a missing package, you need to comment the
# section in xess-VHDL-lib/SDCard.vhd that belongs to 'SdCardCtrlTest' entity.
set_global_assignment -name VHDL_FILE xess-VHDL-lib/SDCard.vhd -library XESS
