library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;

library RFID;
use RFID.UART.all;
use RFID.reciever.all;

library camera;
use camera.utils.all;

use work.utils.all;

package components is
	component RFID_RAM
		generic(
			users:integer:=RFID_users
		);
		port (
			clock	:in std_logic;
			writing	:in boolean;
			address	:in integer range 0 to users-1;
			datain	:in std_logic_vector(40-1 downto 0);
			dataout	:out std_logic_vector(40-1 downto 0)
		);
	end component;
	component PLL
		port(
			areset	:in std_logic:='0';
			inclk0	:in std_logic:='0';
			c0		:out std_logic;
			c1		:out std_logic;
			c2		:out std_logic;
			c3		:out std_logic
		);
	end component;
	component image_ram
		port(
			data		:in std_logic_vector(23 downto 0);
			rdaddress	:in std_logic_vector(16 downto 0);
			rdclock		:in std_logic;
			rden		:in std_logic:='1';
			wraddress	:in std_logic_vector(16 downto 0);
			wrclock		:in std_logic:='1';
			wren		:in std_logic:='0';
			q			:out std_logic_vector(23 downto 0)
		);
	end component;
end package;
