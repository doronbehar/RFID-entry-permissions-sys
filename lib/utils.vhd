library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;

library camera;
use camera.utils.all;

library LCD;
use LCD.utils.all;

package utils is
	-- for user input:
	type button_type is record
		current:std_logic;
		last:std_logic;
	end record;
	constant RFID_users:integer range 0 to 20:=20;
	type RF_broadcast_type is record
		current_state:std_logic;
		last_state:std_logic;
	end record;
	type RF_RAM_type is record
		address:integer range 0 to 20;
		writing:boolean;
		datain:std_logic_vector(40-1 downto 0);
		dataout:std_logic_vector(40-1 downto 0);
	end record;
	type RF_ID_type is record
		current:std_logic_vector(40-1 downto 0);
		buf:std_logic_vector(40-1 downto 0);
	end record;
	type RF_type is record
		ID:RF_ID_type;
		valid:std_logic;
		data:std_logic;
		broadcast:RF_broadcast_type;
		timer:integer range 0 to 10;
		RAM:RF_RAM_type;
	end record;
	type SD_HS_type is record
		i:std_logic; -- High when host has data to give or has taken data.
		o:std_logic; -- High when controller has taken data or has data to give.
	end record;
	type SD_type is record
		reset		:std_logic;						-- active-high, synchronous  reset.
		read		:std_logic;						-- active-high read block request.
		write		:std_logic;						-- active-high write block request.
		continue	:std_logic;						-- If true, inc address and continue R/W.
		address		:std_logic_vector(31 downto 0);	-- Block address.
		datain		:std_logic_vector(7 downto 0);	-- Data to write to block.
		dataout		:std_logic_vector(7 downto 0);	-- Data read from block.
		busy		:std_logic;						-- High when controller is busy performing some operation.
		HS			:SD_HS_type;
		err			:std_logic_vector(15 downto 0);
	end record;
	type touch_row_type is array(1 downto 0) of std_logic_vector(9 downto 0);
	type touch_column_type is array(1 downto 0) of std_logic_vector(8 downto 0);
	type touch_type is record
		row:touch_row_type;
		column:touch_column_type;
		active:button_type;
		fingers:std_logic_vector(1 downto 0);
		gesture:std_logic_vector(7 downto 0);
		delayer:integer range 0 to 7;
	end record;
	type displaying_type is (capture,log,img);
		-- for control:
	type cam_ctrl_type is record
		capture			:std_logic;
		end_capture		:std_logic;
		exposure_adjust	:std_logic;
		invert_color	:std_logic;
		exposure_dec_p	:std_logic;
	end record;
	type cam_type is record
		-- For displaying what the camera sees:
		display		:LCD.utils.screen_type;
		-- For controlling the camera
		ctrl		:cam_ctrl_type;
		-- For internal signals between the camera's driver original sub modules
		read		:camera.utils.read_type;
		count		:camera.utils.count_type;
		reset		:camera.utils.reset_type;
		CCD			:camera.utils.CCD_type;
		-- power on start
		auto_start	:std_logic;
		-- for LCD display
		enable		:std_logic;
	end record;
	type image_RAM_type is record
		data		:std_logic_vector(23 downto 0);
		rdaddress	:std_logic_vector(16 downto 0);
		rdclock		:std_logic;
		rden		:std_logic;
		wraddress	:std_logic_vector(16 downto 0);
		wrclock		:std_logic;
		wren		:std_logic;
		q			:std_logic_vector(23 downto 0);
	end record;
	type image_type is record
		RAM:image_RAM_type;
		red:std_logic_vector(7 downto 0);
		green:std_logic_vector(7 downto 0);
		blue:std_logic_vector(7 downto 0);
	end record;
end package;
